# Change Log

## [Unreleased]
No upcoming changes

## [0.5.4] - 2022-12-13
Reworked once again a large part of the code to make possible simple additions to the configuration.
### Added
- now `launch.json` will be populated with a configuration to add a new target
- new target addition using interactive dialogs

## [0.4.0] - 2022-01-25
Reworked most of the code to make the extension simpler
### Fixed
- can 'handle' comments in the `tasks.json` file (see documentation)

## [0.3.0] - 2021-07-17
Fixed some bugs in the parsing of config files and in their management,
This is the first usable version
### Fixed
- `launch.json` overwritten in some circumstances
### Added
- `tasks.json` systematically completed on the fly

## [0.0.1] - 2021-02-28
Initial release
### Added
- Debug configuration and Tasks provider
- `launch.json` and `tasks.json` generation
