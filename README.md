# make + lldb Configuration

This extension generates debug and tasks configurations both on the fly and as `tasks.json` and `launch.json` files in order to compile using `make` and debug using `lldb` (requires the `CodeLLDB` extension).
It is designed for plain UNIX development environments (not cygwin) with make and lldb installed. The configuration setup VSCode, either :
- to compile the program whose name is the basename of the currently selected file and execute it without arguments
- or to compile the target provided by the user and execute it with the arguments given by the user
In the second form, a new launch configuration is created that enable the reexecution of the given target with the same arguments.

## Features

- provides both debug configurations and compilation tasks
- generates `launch.json` and `tasks.json`
- UNIX only, compile using make, debug using lldb
- let the user create simply its own compilation/debug configurations

## Run target

![Choose your run configuration](https://gricad-gitlab.univ-grenoble-alpes.fr/projet/vscode-images/-/raw/master/Target%20selection.png)

The extension let the user create interactively a new launch configuration by selection `Run new target` in the run menu.
Afterwards this new configuration becomes accessible through the same menu.

## Release Notes

The latest release is still a prototype that has not yet been tested at scale. When adding a configuration to an existing `launch.json` file, `tasks.json` is not generated until the first execution : this is due to the way VSCode generates the new configuration (statically from a snippet). This extension removes comments from `launch.json` when it has to complete it, keeping comments would make the extension much more complex for a very small gain.
